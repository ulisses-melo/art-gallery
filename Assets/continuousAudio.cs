﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class continuousAudio : MonoBehaviour
{
    public AudioSource audio;

    public bool isPLaying = false;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnMouseDown()
    {
        if (isPLaying)
        {
            this.stopSoundEffect();
            isPLaying = false;
            return;
        }

        isPLaying = true;
        this.playSoundEffect();
    }

    void playSoundEffect()
    {
        audio.Play();
    }

    void stopSoundEffect()
    {
        audio.Stop();
    }
}
