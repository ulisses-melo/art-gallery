﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class particlesOnClick : MonoBehaviour
{
    [SerializeField]
    private ParticleSystem particle;

    public bool isActive = false;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnMouseDown()
    {
        if (isActive)
        {
            this.deactivateParticle();
            isActive = false;
            return;
        }

        isActive = true;
        this.activateParticle();
    }

    void activateParticle()
    {
        particle.Play();
    }

    void deactivateParticle()
    {
        particle.Stop();
    }
}
