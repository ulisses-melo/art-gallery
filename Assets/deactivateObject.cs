using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class deactivateObject : MonoBehaviour
{
    public GameObject selectedObject;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnMouseDown()
    {
        this.setInactiveObject();
    }

    void setInactiveObject() 
    {
        selectedObject.gameObject.SetActive(false);
    }
}
